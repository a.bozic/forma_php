<!DOCTYPE HTML>
<html>
<head>
</head>
<body>

<?php

// define variables and set to empty values
$nameErr = $dateErr = $emailErr = $genderErr = $websiteErr = "";
$name = $date = $email = $gender = $comment = $website = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_POST["name"])) {
            $nameErr = "Name is required";
        } else {
            $name = test_input($_POST["name"]);
            }
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                $nameErr = "Only letters and white space allowed";
            }
            if (strlen($name)<3) {
                $nameErr = "Name must contain at least 2 characters";
            }


        if (empty($_POST["date"])) {
            $dateErr = "Name is required";
        } else {
            $date = test_input($_POST["date"]);
        }

        if (empty($_POST["email"])) {
            $emailErr = "Email is required";
        }
        else {
            $email = test_input($_POST["email"]);  }
        // check if e-mail address is well-formed
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
            }
            if (strlen($email)<7) {
                $emailErr = "Email address must contain at least 6 characters";
            }
            if (checkEmailAddress($email) == false) {
                $emailErr = "Email must be from gmail account";
            }
            

        if (empty($_POST["website"])) {
            $website = "";
        } else {
            $website = test_input($_POST["website"]);
            // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
                $websiteErr = "Invalid URL";
            }
        }

        if (empty($_POST["comment"])) {
            $comment = "";
        } else {
            $comment = test_input($_POST["comment"]);
        }

        if (empty($_POST["gender"])) {
            $genderErr = "Gender is required";
        } else {
            $gender = test_input($_POST["gender"]);
        }

    if (!empty($nameErr) or !empty($dateErr) or !empty($emailErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr)) {
        $params = "name=" . urlencode($_POST["name"]);
        $params .= "&date=" . urlencode($_POST["date"]);
        $params .= "&email=" . urlencode($_POST["email"]);
        $params .= "&website=" . urlencode($_POST["website"]);
        $params .= "&comment=" . urlencode($_POST["comment"]);
        $params .= "&gender=" . urlencode($_POST["gender"]);

        $params .= "&nameErr=" . urlencode($nameErr);
        $params .= "&dateErr=" . urlencode($dateErr);
        $params .= "&emailErr=" . urlencode($emailErr);
        $params .= "&websiteErr=" . urlencode($websiteErr);
        $params .= "&commentErr=" . urlencode($commentErr);
        $params .= "&genderErr=" . urlencode($genderErr);

        header("Location: index.php?" . $params);
    }  else {
        echo "<h2>Your Input:</h2>";
        echo "Name: " . $_POST['name'];
        echo "<br>";


        $datenew = date('l', strtotime($_POST['date']));
        echo "Date of birth: " . $datenew. " ".$_POST['date'];
        echo "<br>";


        echo "Email: " . $_POST['email'];
        echo "<br>";

        echo "Website: " . $_POST['website'];
        echo "<br>";

        echo "Comment: " . $_POST['comment'];
        echo "<br>";

        echo "Gender: " . $_POST['gender'];
        echo "<br>";
        echo "<br>";

        echo "<a href=\"index.php\">Return to form</a>";
    }
    }


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
function checkEmailAddress($email) {
    if (strpos($email,"@gmail.com", true)) {
        return true;
    }
    else {
        return false;
    }
}


?>

</body>
</html>